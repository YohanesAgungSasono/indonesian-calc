# Indonesian Calc
	this app will procces the indonesian sentence into arithmetic operation and give the result.

# How to run
	copy Calc folder to htdocs (if you use xampp), then run it from your browser localhost/Calc
	this will process your indonesian arithmetic sentence to numeric operation.
	
	ex :
	input : satu kali dua ( means 1 x 2),it will gives result 'satu kali dua adalah dua' (means 1x2 = 2)
			dua puluh tambah dua ( means 20 + 2), it will gives result 'dua puluh tambah dua adalah dua puluh dua' (means 20+2 = 22)
	
	list of operator :
	tambah (addition)
	kali (multiplication)
	bagi (division)
	kurang (subtraction)