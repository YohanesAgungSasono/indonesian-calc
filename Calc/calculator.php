<?php
class Calculator{
	public $StrInput;
	private $Operator;
	private $OperatorPos;
	private $StrFirst;
	private $StrSecond;
	private $result;
	public $StrRes;
	private $error = 'false';
	private $Numbers = [ 'nol'=>0, 'satu'=>1, 'dua'=>2, 'tiga'=>3, 'empat'=>4, 'lima'=>5, 
						'enam'=>6, 'tujuh'=>7, 'delapan'=>8, 'sembilan' =>9, 'sepuluh'=>10,
						'sebelas'=>11, 'koma'=>'.'];
	private $Suffixs = ['belas'=>10,'puluh'=>1,'ratus'=>2,'ribu'=>3];
	private $Operators = ['tambah','kurang','kali','bagi'];		
	public function __construct($Input){
		$this->StrInput = explode(" ", strtolower(trim($Input)));
	}
	private function getOperatorAndStringNumbers(){
		$counter = 0;
		foreach($this->Operators as $Operation){
			if(in_array($Operation, $this->StrInput)){
				$counter++;
				$this->Operator = $Operation;
				$this->OperatorPos = array_search($Operation,$this->StrInput);
			}
		}
		if ($counter > 1 || $counter == 0){
			$this->error = 'saat ini hanya support 1 arithmetic operasi saja, jumlah operator anda'.$counter;
			unset($counter);
			return $this->error;
		}
		else if(count($this->StrInput) === ($this->OperatorPos +1)){
			$this->error = 'operasi arithmmatic kurang lengkap';
			unset($counter);
			return $this->error;
		}
		unset($counter);
		$this->StrFirst = array_slice($this->StrInput,0,$this->OperatorPos);
		$this->StrSecond = array_slice($this->StrInput,($this->OperatorPos+1));
			
	}
	private function StrToNumber($InputNumber){	
		$output = 0;
		if(count($InputNumber)>3){
			$this->error = "angka tidak dikenali";
			return $this->error;
		}
		try{
			foreach($InputNumber as $num){
				if(in_array($num,array_flip($this->Numbers)))
					$output = $output + $this->Numbers[$num];
				else if(in_array($num,array_flip($this->Suffixs))){						
					if($num === 'belas'){
						if(count($InputNumber)>2){
							throw new Exception("error");
						}
						else{
							$output = $output + $this->Suffixs[$num];
						}
					}
					else
						$output = $output * (pow(10,$this->Suffixs[$num]));
				}
			}
			return $output;
		}
		catch (Exception $e) {
			$this->error = "angka tidak dikenali";
			return $this->error;
		}
	}
	private function getFirstNumber(){
		return $this->StrToNumber($this->StrFirst);
	}
	private function getSecNumber(){
		return $this->StrToNumber($this->StrSecond);
	}
	private function CalcAdd($a, $b){
		return ($a+$b);
	}
	private function CalcMinus($a, $b){
		return ($a-$b);
	}
	private function CalcMultiply($a, $b){
		return ($a*$b);
	}
	private function CalcDevide($a, $b){
		try{
			return round(($a/$b),2);
		}
		catch(Exception $e){
			$this->error = $e;
		}
	}
	protected function CalcProceed(){
		$First;
		$Second;
		try{
			$this->getOperatorAndStringNumbers();
			$First = $this->getFirstNumber();
			$Second = $this->getSecNumber();				
		}
		catch(Exception $e){
			return $this->error;
		}
		if($this->error !== 'false')
			return $this->error;
		try{
			switch($this->Operator){
				case 'tambah':
					$this->result = $this->CalcAdd($First,$Second);
					break;
				case 'kurang':
					$this->result = $this->CalcMinus($First,$Second);
					break;
				case 'kali':
					$this->result = $this->CalcMultiply($First, $Second);
					break;
				case 'bagi':
					$this->result = $this->CalcDevide($First, $Second);
					break;
			}
		}
		catch(Exception $e){
			$this->error = $e;
			return $e;
		}
		if($this->error === 'false')
		{
			return $this->result;
		}
		else 
			return $this->error;
	}
	private function NumberToStr($arr_number){
		if($arr_number[0] == '-'){
			$this->StrRes.= 'negatif ';
			array_shift($arr_number);
		}
		if(count($arr_number)==1){
			if(in_array($arr_number[0],$this->Numbers))
				$this->StrRes .= array_search($arr_number[0],$this->Numbers);
			else
				$this->error = 'oops, silahkan cek lagi bilangan input';
		}
		else{			
			foreach($arr_number as $eNum){
				if($eNum == 0){
					array_shift($arr_number);
					continue;
				}
				if(count($arr_number)==2)
				{					
					$CheckNum = implode($arr_number);
					if(in_array($CheckNum,$this->Numbers)){ 
						$this->StrRes.= array_search($CheckNum,$this->Numbers);
						break;
					}
					if($arr_number[0]==1)
					{						
						$this->StrRes.=array_search($arr_number[1],$this->Numbers).' belas';
						break;
					}
					elseif(in_array($eNum,$this->Numbers)){
						$this->StrRes.= array_search($eNum,$this->Numbers).' ';
						$this->StrRes.=array_search((count($arr_number)-1),$this->Suffixs).' ';
						array_shift($arr_number);
					}
					else
						$this->error = 'Oops, silahkan cek kembali bilangan input';
				}
				else
				{
					if(in_array($eNum,$this->Numbers)){
						if((count($arr_number)==1)){
							$this->StrRes.=array_search($eNum,$this->Numbers);
							break;
						}
						else{
							if($eNum == 1)
								$this->StrRes.='se';
							else
								$this->StrRes.=array_search($eNum,$this->Numbers).' ';
							$this->StrRes.=array_search((count($arr_number)-1),$this->Suffixs).' ';
						}
						array_shift($arr_number);
					}
				}					
			}
		}
		if($this->error === 'false')
			return $this->StrRes;
		else
			return $this->error;
	}
	private function NumberToStrAfterDot($arr_number){
		foreach($arr_number as $eNum){
			if(in_array($eNum,$this->Numbers))
				$this->StrRes .= array_search($eNum,$this->Numbers).' ';
			else{
				$this->error = 'Oops, silahkan cek lagi bilangan input';
				return $this->error;
			}
		}
		return $this->StrRes;
	}
	public function GetResult(){
		if($this->error === 'false')
		{
			$Res = $this->CalcProceed();
			$Splitted = str_split($Res);
			unset($Res);
			if(in_array('.', $Splitted)){
				$pos = in_array('.', $Splitted);
				$this->StrRes = $this->NumberToStr(array_slice($Splitted,0,$pos));
				$this->StrRes .=' koma ';
				$this->StrRes = $this->NumberToStrAfterDot(array_slice($Splitted,($pos+1)));
				unset($pos);
				return $this->StrRes;
			}
			else
				return $this->NumberToStr($Splitted);
		}
		else
			return $this->error;
	}
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{	
	$postdata = file_get_contents("php://input");
	$Calc = new Calculator($postdata);
	//$Calc = new Calculator('sembilan puluh sembilan kali sembilan puluh sembilan ');
	$TextInput = $Calc->StrInput;
	$TextResult = $Calc->GetResult();
	unset($Calc);
	echo implode(' ',$TextInput)." adalah ".$TextResult;
}
?>